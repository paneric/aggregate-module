<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Repository;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Query;
use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;

class ModuleQuery extends Query implements ModuleQueryInterface
{
    public function __construct(Manager $manager, array $config)
    {
        parent::__construct($manager);

        $this->adaoClass = $config['adao_class'];

        $this->baseQuery = $config['base_query'];
    }
}
