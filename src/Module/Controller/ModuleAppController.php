<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Controller;

use Paneric\AggregateModule\Interfaces\Action\App\GetAllByPaginatedAppActionInterface;
use Paneric\CSRTriad\Controller\AppController;
use Paneric\AggregateModule\Interfaces\Action\App\GetAllPaginatedAppActionInterface;
use Paneric\AggregateModule\Interfaces\Action\App\GetOneByIdsAppActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ModuleAppController extends AppController
{
    protected $routePrefix;

    public function __construct(Twig $twig, string $routePrefix)
    {
        parent::__construct($twig);

        $this->routePrefix = $routePrefix;
    }

    public function showAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedAppActionInterface $action,
        string $page = null
    ): Response {
        return $this->render(
            $response,
            '@module/show_all_paginated.html.twig',
            $action->getAllPaginated($request, $page)
        );
    }

    public function showAllByPaginated(
        Request $request,
        Response $response,
        GetAllByPaginatedAppActionInterface $action,
        String $relation,
        String $id,
        string $page = null
    ): Response {
        return $this->render(
            $response,
            '@module/show_all_by_paginated.html.twig',
            $action->getAllByPaginated($request, $relation, $id, $page)
        );
    }

    public function showOneByIds(
        Response $response,
        GetOneByIdsAppActionInterface $action,
        String $leftId,
        String $rightId
    ): Response {
        return $this->render(
            $response,
            '@module/show_one_by_ids.html.twig',
            $action->getOneByIds($leftId, $rightId),
        );
    }
}
