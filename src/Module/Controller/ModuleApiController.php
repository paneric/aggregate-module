<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Controller;

use Paneric\AggregateModule\Interfaces\Action\Api\GetAllByPaginatedApiActionInterface;
use Paneric\CSRTriad\Controller\ApiController;
use Paneric\AggregateModule\Interfaces\Action\Api\GetAllPaginatedApiActionInterface;
use Paneric\AggregateModule\Interfaces\Action\Api\GetOneByIdsApiActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ModuleApiController extends ApiController
{
    protected $dbPrefix;

    public function __construct(string $dbPrefix)
    {
        $this->dbPrefix = $dbPrefix;
    }

    public function getAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApiActionInterface $action,
        string $page
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->getAllPaginated($request, $page),
            $action->getStatus()
        );
    }

    public function getAllByPaginated(
        Request $request,
        Response $response,
        GetAllByPaginatedApiActionInterface $action,
        string $relation,
        string $id,
        string $page
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->getAllByPaginated($request, $relation, $id, $page),
            $action->getStatus()
        );
    }

    public function getOneByIds(
        Response $response,
        GetOneByIdsApiActionInterface $action,
        String $leftId,
        String $rightId
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->getOneByIds($leftId, $rightId),
            $action->getStatus()
        );
    }
}
