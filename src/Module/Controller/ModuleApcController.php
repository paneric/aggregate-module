<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Controller;

use Paneric\AggregateModule\Interfaces\Action\Apc\GetAllByPaginatedApcActionInterface;
use Paneric\CSRTriad\Controller\AppController;
use Paneric\AggregateModule\Interfaces\Action\Apc\GetAllPaginatedApcActionInterface;
use Paneric\AggregateModule\Interfaces\Action\Apc\GetOneByIdsApcActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ModuleApcController extends AppController
{
    protected $routePrefix;

    public function __construct(Twig $twig, string $routePrefix)
    {
        parent::__construct($twig);

        $this->routePrefix = $routePrefix;
    }

    public function showAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApcActionInterface $action,
        string $page = null
    ): Response {
        return $this->render(
            $response,
            '@module/show_all_paginated.html.twig',
            $action->getAllPaginated($request, $page),
        );
    }

    public function showAllByPaginated(
        Request $request,
        Response $response,
        GetAllByPaginatedApcActionInterface $action,
        string $relation,
        string $id,
        string $page = null
    ): Response {
        return $this->render(
            $response,
            '@module/show_all_by_paginated.html.twig',
            $action->getAllByPaginated($request, $relation, $id, $page),
        );
    }

    public function showOneByIds(
        Request $request,
        Response $response,
        GetOneByIdsApcActionInterface $action,
        string $leftId,
        string $rightId
    ): Response {
        return $this->render(
            $response,
            '@module/show_one_by_ids.html.twig',
            $action->getOneByIds($request, $leftId,$rightId)
        );
    }
}
