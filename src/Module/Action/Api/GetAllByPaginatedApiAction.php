<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Action\Api;

use Paneric\AggregateModule\Interfaces\Action\Api\GetAllByPaginatedApiActionInterface;
use Paneric\CSRTriad\Service;
use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;


class GetAllByPaginatedApiAction extends Service implements GetAllByPaginatedApiActionInterface
{
    protected $adapter;

    protected $findByCriteria;
    protected $orderBy;

    protected $status;

    public function __construct(ModuleQueryInterface $adapter, array $config)
    {
        parent::__construct();

        $this->adapter = $adapter;

        $this->findByCriteria = $config['find_by_criteria'];
        $this->orderBy = $config['order_by'];
    }

    public function getAllByPaginated(Request $request, string $relation, string $id, string $page): array
    {
        $pagination = $request->getAttribute('pagination');

        $queryParams = $request->getQueryParams();

        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $collection = $this->adapter->queryBy(
            $findByCriteria($relation, $id),
            $orderBy(),
            $pagination['limit'],
            $pagination['offset']
        );

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->jsonSerializeObjects($collection, true),
            'pagination' => $pagination,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
