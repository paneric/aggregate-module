<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\AggregateModule\Interfaces\Action\Api\GetOneByIdsApiActionInterface;
use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;

class GetOneByIdsApiAction extends Service implements GetOneByIdsApiActionInterface
{
    protected $adapter;

    protected $findOneByCriteria;

    protected $status;

    public function __construct(ModuleQueryInterface $adapter, array $config)
    {
        parent::__construct();

        $this->adapter = $adapter;

        $this->findOneByCriteria = $config['find_one_by_criteria'];
    }

    public function getOneByIds(String $leftId, String $rightId): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        $adto = $this->adapter->queryOneBy($findOneByCriteria($leftId, $rightId));

        if ($adto ===  null) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Resource not found'
            ];
        }

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $adto->transform(),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
