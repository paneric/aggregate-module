<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\AggregateModule\Interfaces\Action\App\GetOneByIdsAppActionInterface;
use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetOneByIdsAppAction extends Service implements GetOneByIdsAppActionInterface
{
    protected $adapter;

    protected $moduleNameSc;

    protected $findOneByCriteria;

    protected $prefix;

    public function __construct(ModuleQueryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->moduleNameSc = $config['module_name_sc'];

        $this->findOneByCriteria = $config['find_one_by_criteria'];

        $this->prefix = $config['prefix'];
    }

    public function getOneByIds(String $leftId, String $rightId): ?array
    {
        $this->session->setFlash([
            'page_title' => sprintf(
                'content_%s_show_one_by_ids_title',
                $this->moduleNameSc
            )
        ], 'value');

        $findOneByCriteria = $this->findOneByCriteria;

        $dto = $this->adapter->queryOneBy(
            $findOneByCriteria($leftId, $rightId)
        );

        if ($dto ===  null) {
            return null;
        }

        return [
            $this->prefix => $dto->convert()
        ];
    }
}
