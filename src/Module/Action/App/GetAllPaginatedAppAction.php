<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\AggregateModule\Interfaces\Action\App\GetAllPaginatedAppActionInterface;
use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedAppAction extends Service implements GetAllPaginatedAppActionInterface
{
    protected $adapter;

    protected $moduleNameSc;
    protected $findByCriteria;
    protected $orderBy;

    protected $prefix;

    public function __construct(ModuleQueryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->findByCriteria = $config['find_by_criteria'];
        $this->orderBy = $config['order_by'];
        $this->moduleNameSc = $config['module_name_sc'];

        $this->prefix = $config['prefix'];
    }

    public function getAllPaginated(Request $request, string $page = null): array
    {
        $pagination = $request->getAttribute('pagination');

        $this->session->setData($pagination, 'pagination');

        $this->session->setFlash(['page_title' => 'content_' . $this->moduleNameSc . '_show_title'], 'value');

        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $collection = $this->adapter->queryBy(
            $findByCriteria(),
            $orderBy(),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            $this->prefix . 's' => $this->jsonSerializeObjects($collection, true)//true if aggregates
        ];
    }
}
