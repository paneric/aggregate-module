<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Action\App;

use Paneric\AggregateModule\Interfaces\Action\App\GetAllByPaginatedAppActionInterface;
use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllByPaginatedAppAction extends Service implements GetAllByPaginatedAppActionInterface
{
    protected $adapter;

    protected $moduleNameSc;
    protected $orderBy;
    protected $findByCriteria;

    protected $prefix;

    public function __construct(ModuleQueryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->orderBy = $config['order_by'];
        $this->findByCriteria = $config['find_by_criteria'];
        $this->moduleNameSc = $config['module_name_sc'];

        $this->prefix = $config['prefix'];
    }

    public function getAllByPaginated(Request $request, String $relation, String $id, String $page = null): array
    {
        $pagination = $request->getAttribute('pagination');

        $this->session->setData($pagination, 'pagination');

        $this->session->setFlash([
            'page_title' => sprintf(
                'content_%s_show_by_%s_title',
                $this->moduleNameSc,
                $relation
            )
        ], 'value');


        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $collection = $this->adapter->queryBy(
            array_merge(
                [$this->prefix . '_' . $relation . '_id' => $id],
                $findByCriteria()
            ),
            $orderBy(),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            $this->prefix . 's' => $this->jsonSerializeObjects($collection, true)//true if aggregates
        ];
    }
}
