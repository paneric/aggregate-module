<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Module\Action\Apc;

use Paneric\AggregateModule\Interfaces\Action\Apc\GetAllByPaginatedApcActionInterface;
use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllByPaginatedApcAction extends Service implements GetAllByPaginatedApcActionInterface
{
    protected $manager;
    private $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function getAllByPaginated(Request $request, string $relation, string $id, string $page = null): array
    {
        $this->session->setFlash(
            ['page_title' => 'content_' . $this->config['module_name_sc'] . '_show_title'],
            'value'
        );

        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $request->getAttribute('token'),
            ],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $getAllByPaginatedUri = sprintf(
            '%s%s/%s/',
            $this->config['get_all_by_paginated_uri'],
            $relation,
            $id
        );

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s%s',
                $this->config['base_url'],
                $getAllByPaginatedUri,
                $page ?? '1'
            ),
            $options
        );

        $jsonResponse['pagination']['path'] = sprintf(
            '%s%s/%s/',
            $this->config['origin_get_all_by_paginated_uri'],
            $relation,
            $id
        );

        if ($jsonResponse['status'] === 200) {
            $this->session->setData($jsonResponse['pagination'], 'pagination');

            return [
                $this->config['prefix'] . 's' => $jsonResponse['body']
            ];
        }

        return [];
    }
}
