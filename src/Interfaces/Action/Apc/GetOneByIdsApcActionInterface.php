<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\Apc;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetOneByIdsApcActionInterface
{
    public function getOneByIds(Request $request, String $leftId, String $rightId): ?array;
}
