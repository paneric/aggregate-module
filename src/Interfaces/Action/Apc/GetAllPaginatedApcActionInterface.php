<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\Apc;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllPaginatedApcActionInterface
{
    public function getAllPaginated(Request $request, string $page = null): array;
}
