<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\Api;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllByPaginatedApiActionInterface
{
    public function getAllByPaginated(Request $request, string $relation, string $id, string $page): array;
    public function getStatus(): int;
}
