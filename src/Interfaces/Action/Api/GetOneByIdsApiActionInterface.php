<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\Api;

interface GetOneByIdsApiActionInterface
{
    public function getOneByIds(String $leftId, String $rightId): ?array;
    public function getStatus(): int;
}
