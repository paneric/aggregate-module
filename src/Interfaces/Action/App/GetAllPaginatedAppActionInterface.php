<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\App;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllPaginatedAppActionInterface
{
    public function getAllPaginated(Request $request, string $page = null): array;
}
