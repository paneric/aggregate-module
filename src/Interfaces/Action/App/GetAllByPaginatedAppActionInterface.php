<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\App;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllByPaginatedAppActionInterface
{
    public function getAllByPaginated(Request $request, string $relation, string $id, string $page = null): array;
}
