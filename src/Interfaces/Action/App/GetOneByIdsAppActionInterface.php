<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Action\App;

interface GetOneByIdsAppActionInterface
{
    public function getOneByIds(String $leftId, String $rightId): ?array;
}
