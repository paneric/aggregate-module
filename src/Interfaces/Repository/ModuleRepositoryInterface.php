<?php

declare(strict_types=1);

namespace Paneric\AggregateModule\Interfaces\Repository;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface ModuleRepositoryInterface
{
    public function findOneBy(array $criteria): ?DataObjectInterface;

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function createUnique(array $criteria, DataObjectInterface $dataObject): ?string;
    public function updateUnique(array $criteriaSelect, array $criteriaUpdate, DataObjectInterface $dataObject): ?int;
    public function delete(array $criteria): int;

    public function getRowsNumber(): int;

    public function findAll(): array;
}
